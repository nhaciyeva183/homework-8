import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {

        Human mother = new Human("Anna", "Bell", 35);
        Human father = new Human("George", "Bell", 40);

        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        habits.add("drink");
        DomesticCat cat = new DomesticCat("Kiwi", 2, 60, habits);
        //System.out.println(cat);

        Map<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY.name(), "do home work");
        schedule.put(DayOfWeek.MONDAY.name(), " go to courses");
        schedule.put(DayOfWeek.TUESDAY.name(), "watch a film");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "read a book");
        schedule.put(DayOfWeek.THURSDAY.name(), "go shopping");
        schedule.put(DayOfWeek.FRIDAY.name(), "complete course videos");
        schedule.put(DayOfWeek.SATURDAY.name(), "join online classes");
        Human child = new Human("Mary", "Bell", 15, 100, cat, schedule);
        //System.out.println(child.toString());

        Human child2 = new Human("Maryam", "Bell", 10);
        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);


        Family family = new Family(mother, father, children);
        //System.out.println(family);

        Human child3 = new Human("John", "Bell", 7);
        family.addChild(family,child3);

        System.out.println("The all members of family:" + family.familyMembers(family));
        System.out.println("--------------------------------------------");

//        family.deleteChildByIndex(family, 1);
        family.deleteChild(family, child);
        System.out.println("After deleting 1 child by the index :" + family.familyMembers(family));
        System.out.println("--------------------------------------------");
//        System.out.println(family);

    }
}
