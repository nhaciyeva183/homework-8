import java.util.Set;

public class Dog extends Pet {
    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void eat() {
        System.out.println("Dogs are eating");
    }

    @Override
    public void respond() {
        System.out.println("Some dogs are incredible swimmers");
    }
}
