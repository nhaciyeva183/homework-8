import java.util.Set;

public class DomesticCat extends Pet {

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

    }

    @Override
    public void eat() {
        System.out.println("Domestic cats are eating");
    }

    @Override
    public void respond() {
        System.out.println("Domestic cats primarily live in areas of human habitation");
    }
}
