public class Woman extends Human {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet(Pet pet) {
        super.greetPet(pet);
    }

    public void makeup() {
        System.out.println("I did my makeup");
    }
}
