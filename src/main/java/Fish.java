import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void eat() {
        System.out.println("Fish is eating");
    }

    @Override
    public void respond() {
        System.out.println("Fish are aquatic, craniate");
    }
}
