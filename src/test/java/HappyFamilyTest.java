import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HappyFamilyTest {

    Human mother = new Human("Anna", "Bell", 35);
    Human father = new Human("George", "Bell", 40);

    Human child = new Human("Mary", "Bell", 15);
    Human child2 = new Human("Maryam", "Bell", 10);
    Human child3 = new Human("John", "Bell", 7);

    @Test
    void addChild(){
        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        Family family = new Family(mother, father, children);
        Assertions.assertTrue(family.addChild(family, child3));
    }

    @Test
    void familyMembers(){
        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        Family family = new Family(mother, father, children);
        family.addChild(family, child3);
        Assertions.assertEquals(5, family.familyMembers(family));
    }

    @Test
    void deleteChild(){
        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children);
        int count = family.familyMembers(family);
        family.deleteChild(family, child2);
        Assertions.assertEquals(count-1, family.familyMembers(family));
    }

    @Test
    void deleteChildByIndex(){
        List<Human> children = new ArrayList<>();
        children.add(child);
        children.add(child2);
        children.add(child3);
        Family family = new Family(mother, father, children);
        int count = family.familyMembers(family);
        family.deleteChildByIndex(family, 0);
        Assertions.assertEquals(count-1, family.familyMembers(family));
    }

}